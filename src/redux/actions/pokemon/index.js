import { BASE_URL_API } from "../../../api/api";
export const getListData=()=>{
  return dispatch => {
    return BASE_URL_API.get('pokemon/ditto').then(response =>{
      dispatch({
        type: 'GET_LIST',
        payload: response.data,
      })
    })
  }
}