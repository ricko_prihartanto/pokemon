const initialState = {
    data:[],
    success: false
}
const pokemonReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_LIST':
      console.log(action.payload, 'reducer')
      return {...state, data:action.payload, success: true}
    default:
      return state
  }
}

export default pokemonReducer