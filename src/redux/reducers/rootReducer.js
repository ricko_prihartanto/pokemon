// ** Redux Imports
import { combineReducers } from 'redux'

// ** Reducers Imports
import pokemonReducer from './pokemon'

const rootReducer = combineReducers({
  pokemonReducer
})

export default rootReducer