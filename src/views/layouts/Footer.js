import React from "react";
function Footer(props){
    return (
        <div className="Footer">
            <h1>{props.title}</h1>
        </div>
    )
}

export default Footer