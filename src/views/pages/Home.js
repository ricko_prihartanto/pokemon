import React, {useEffect} from "react"
import {useDispatch, useSelector} from "react-redux"
import {getListData} from '../../redux/actions/pokemon/index'
function Home(){
  // ** State
  const PokemonStore = useSelector(state => state.pokemonReducer)
  const dispatch = useDispatch()
  // const [listData, setData] = useState()

  // useEffect(()=>{
  //   dispatch(getListData({}))
  //     console.log(PokemonStore.data)
  //     // setData(PokemonStore.data)
  // }, [PokemonStore])
    return (
      <>
        <main>
          <h2>Welcome to the homepage!</h2>
          <p>You can do this, I believe in you.</p>
        </main>
      </>
    );
  }
  export default Home