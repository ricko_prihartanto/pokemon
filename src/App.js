
import React from 'react';
import Header from "./views/layouts/Header"
import Footer from "./views/layouts/Footer"
import AppRouter from "./router/AppRouter"
function App() {
    return (
      <div className="App">
        <Header title="INI HEADER 1"/>
        <AppRouter/>
        <Footer title="INI FOOTER"/>
      </div>
    );
}
export default App;
