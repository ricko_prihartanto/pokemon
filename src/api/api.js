import axios from 'axios'
export const BASE_URL_API = axios.create ({
    baseURL: process.env.REACT_APP_BASE_URL_API,
    headers: { 
      'Content-Type': 'application/json'
    },
});