import React from "react"
import {BrowserRouter, Routes, Route, Link } from "react-router-dom"
import Home from "../views/pages/Home"
import About from "../views/pages/About"
const Beranda = ()=> <Home/>
const Tentang = ()=> <About/>
function AppRouter() {
    return (
      <BrowserRouter>
        <div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </div>
        <Routes>
          <Route path="/" exact element={<Beranda />} />
          <Route path="/about" element={<Tentang />} />
        </Routes>
      </BrowserRouter>
    )
}
export default AppRouter